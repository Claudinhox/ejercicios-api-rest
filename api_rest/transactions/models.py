from django.db import models

class Transactions(models.Model):
    id = models.IntegerField(primary_key=True, null=False)
    first_name = models.CharField(max_length=100, null=False)
    last_name = models.CharField(max_length=100, null=False)
    email = models.CharField(max_length=100, null=False)
    category = models.CharField(max_length=50, null=False)
    timestamp = models.IntegerField(null=False)
    clicks = models.IntegerField(null=False, default=0)
    opens = models.IntegerField(null=False, default=0)
    status = models.CharField(max_length=100, null=False)