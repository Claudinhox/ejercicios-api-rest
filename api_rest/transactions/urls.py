from django.urls import path
from .views import TransactionsListView, TransactionsDetailView

urlpatterns = [
    path('transactions/', TransactionsListView.as_view(), name='transactions_list'),
    path('transactions/<int:pk>/', TransactionsDetailView.as_view(), name='transactions_detail')
]
