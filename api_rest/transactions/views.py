from django.views import View
from .models import Transactions
from django.http import JsonResponse
from django.forms.models import model_to_dict

class TransactionsListView(View):
    def get(self, request):
        t_list = Transactions.objects.all()
        return JsonResponse(list(t_list.values()), safe=False)

class TransactionsDetailView(View):
    def get(self, request, pk):
        t_detail = Transactions.objects.get(pk=pk)
        return JsonResponse(model_to_dict(t_detail))